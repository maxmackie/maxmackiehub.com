## Version

I tend to change my website quite a bit (I promise to do it less, I'll probably test stuff in a sandbox instead). For this reason, I'll be keeping past frameworks (HTML/CSS) in a directory named `_past_versions`.

## License

_As of March27th, 2012, my licensing scheme has changed. Please see below._

I release everything on my blog under a the [Attribution-NonCommercial-ShareAlike 2.5 Generic](http://creativecommons.org/licenses/by-nc-sa/2.5/) license. This means you can take anything you find (my content, my styles, my images, etc) on my website and use, copy, change it as long as you link back to me in some way and redistribute the new content under the same or a similar license.  

## Jekyll

My website/blog is powered by [Jekyll](http://jekyllbootstrap.com) and hosted by Github. Thanks to all of you for such great software and code hosting. Jekyll is released under a [Creative Commons](http://creativecommons.org/licenses/by-nc-sa/3.0/) license.
