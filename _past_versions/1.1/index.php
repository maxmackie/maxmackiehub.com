<html>
    <head>
        <title>Max Mackie</title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id="left-side">
            <div id="topbar"></div>
            <div id="content">
                
            </div>
            
            <div id="profile">
                <img id="avatar" src="http://www.gravatar.com/avatar/2ca46d1393e0337704ce3b91b1c0645a?s=80"/>
            </div>
            <div id="links">
                <p>Hello, my name is <b>Max Mackie</b>.
                I'm currently a web code wrangler at the Bank of Canada (in Ottawa)</p>
                
                <p>You can find me online at <a href="http://github.com/maxmackie">Github</a>
                or on the <a href="http://stackexchange.com/users/310807/maxmackie">StackExchange Network</a></p>
                
                <a href="http://blog.maxmackie.com"><div id="blog">Visit my Blog</div></a>
                
            </div>
        </div>
        <div id="right-side">
            <h1 id="projects">Projects</h1>
                <p>I like keeping busy while learning new tech. This leads me to 
                have many, many ongoing projects at any given time. I've included 
                a few below. Most things I put up on Github.</p>
                
                <p><a href="http://institutum.org/">INSTITUTUM</a><br/>
                This is currently my biggest project. I'm collaborating with Lance Lafontaine to
                create an online academic learning and teaching machine. We've been working for 
                Institutum for about 8 months and are slowly making progress when time permits. The
                website is currently closed to the public, but we hope to get into a private beta
                by the end of 2012.</p>
                
                <p><a href="#">IDEAFORGE</a><br/>
                I have a lot of ideas. Most of the time, they suck. Other times, I know they have
                merit. I found myself needing an easy way to organize my thoughts and ideas, thus
                IdeaForge was born. It is almost ready for deployment (I have been using it for a
                couple months). The source will be on Github.</p>
                
                <p><a href="http://github.com/maxmackie/vssh">VSSH</a><br/>
                This is my first Python project, so development is slow going. vssh is a network
                monitoring tool which runs commands through ssh on various servers you configure.
                The difference is that vssh will display the output (STDOUT) in your local terminal
                for each command run on each server. It is designed to run several
                commands and generate a sort of 'hub' for your monitoring needs.
                </p>
                
                <p><a href="http://maxmackie.com/roar">ROAR (Right on and Ready)</a><br/>
                ROAR is an efficiency model designed to get stuff done. It teaches the user to 
                realize the value of their time, thus working efficiently. The full model is currently
                being developed and a hosted ROAR service is a possibility in the future.
                </p>
                
            <h1 id="learning">Learning</h1>
                <p>Learning is by far one of the most important things in life (see my blog post
                on "How to be a good little human"). I love taking on something new (time permitting)
                and also love sharing new things with people. Here's what I'm currently adding
                to my biological hard drive:</p>
                
                <p>
                    <ul>
                        <li>Learning how to use the CodeIgniter PHP framework (for a project I'm working on)</li>
                        <li>Taking Stantford University's online Cryptography course</li>
                        <li>Looking into iPhone/iPad app development</li>
                        <li>Experimenting with Javascript/jQuery to do some funky web stuff</li>
                        <li>Finding my way around the new HTML5/CSS3</li>
                    </ul>
                </p>
                
            <h1 id="resume">Resume</h1>
            <p>My full resume can be found on <a href="http://careers.stackoverflow.com/maxmackie">
            StackExchange Careers</a> or <a href="http://ca.linkedin.com/in/maxmackie">LinkedIn</a>. 
            A list of references will be provided on contact. I am not currently looking for work (school
            and internships), but send me an email and we can talk.</p>
                
            <h1 id="contact">Contact</h1>
                <p>I am always willing to talk to you! I love having conversations with random
                people about most anything. In the past I've received technical questions, but
                sometimes people are just looking for a friendly chat or an extra pair of eyes
                for a project/code.</p>
                
                <p>Preferred method of contact is email at my personal address: 
                <a href="mailto:talk@f33r.com">talk@f33r.com</a></p>
                
                <p>You can also find me on Freenode under the username MaxMackie</p>
                
            <div id="footer">
                Website design and content are copyright of Maximillien Courchesne-Mackie and released
                under a <a href="http://creativecommons.org/licenses/by-nc-sa/2.5/ca/">Creative Commons license</a>.
            </div>
        </div>
        <div id="controls"></div>
    </body>
</html>