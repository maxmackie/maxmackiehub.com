---
layout: default
---

# Changing terminal colors using .Xdefaults and xcolors
<p class="date">Sunday December 18th, 2011</p>

I'm currently in an almost terminal-only environment on my desktop at home (dwm), which is what forced me to make my terminal pleasing to look at. I'm using UXTerm as my default terminal, but this technique will work for others (xterm too!).

The `.Xdefaults` file, which usually lives in the user's home directory, allows custom settings for certain applications. In this case, I'm going to be setting specific colors to be used inside UXTerm (which will extend to any ASCII-style gui application run inside the terminal too). By default, there are 8 colors used: Black, Red, Green, Yellow, Blue, Magenta, Cyan and White. Here are my modifications:

{% highlight bash %}
! -----------------------------------
! ~/.Xdefaults
! By Max Mackie (max@f33r.com)
! -----------------------------------
    
! [0] black
UXTerm*color0: #242424
UXTerm*color8: #242424
    
! [1] red
UXTerm*color1: firebrick1
UXTerm*color9: firebrick1
    
! [2] green
UXTerm*color2: palegreen
UXTerm*color10: palegreen
    
! [3] yellow
UXTerm*color3: LightGoldenrod1
UXTerm*color11: LightGoldenrod1
   
! [4] blue
UXTerm*color4: SteelBlue1
UXTerm*color12: SteelBlue1
    
! [5] magenta
UXTerm*color5: violet
UXTerm*color13: violet
    
! [6] cyan
UXTerm*color6: CadetBlue2
UXTerm*color14: CadetBlue2
    
! [7] white
UXTerm*color7: white
UXTerm*color15: white
{% endhighlight %}

Each entry (X resource) in this file follows the same syntax:

{% highlight bash %}
application.resource.subresource.subresource.attribute: value
{% endhighlight %}

In my case, I'm changing the default colors used by the UXTerm application. You can use HEX color notation (`#242424`) or you can use X colors. To see what colors X recognizes, open up a terminal and type:

    $ xcolors

This will spawn a screen showing you all the available colors and their associated name. Inserting that name into the `.Xdefaults` file will yield the same result as putting the HEX value. Finally, make sure your `.Xdefaults` is parsed at boot, add this line to your `~/.xinitrc` file:

{% highlight bash %}
xrdb -merge ~/.Xdefaults
{% endhighlight %}
