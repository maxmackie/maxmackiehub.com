---
layout: default
---

# Modifying Posterous' code blocks
<p class="date">Thursday February 9th, 2012</p>

HN has changed my mind about something again: [Why I don't host my own blog anymore](http://news.ycombinator.com/item?id=3571958).

I used to have a Wordpress blog on my own server. It wasn't so bad, but eventually I was unable to keep up with the constant updates coming from both the core application and all my plugins. To be brief: it sucked.

With this in mind I read [Patrick's](http://www.kalzumeus.com/about/) (from Kalzumeus Software) article and while he approaches the issue from a different perspective (he has tons of unique visits and needed to scale appropriately), I was able to get something out of it. I used to shy away from hosted blogs, I guess I didn't feel "cool" enough using what everyone else was.

A couple days later I discovered how awesome Posterous was -- apart from one thing that kept nagging me. I tend to like posting snippets of code and the default look and feel of a code block just didn't do it for me. Digging through the "Customize" screen, I found that there's some embedded CSS that you can play with, but nothing to do with the `<pre>` tag. Thanks to Chrome's "Inspect Element" feature I was able to see that the code blocks were really two nested divs followed by `<pre>` tags. Something like this:

{% highlight html %}
<div class="CodeRay">
    <div class="code">
        <pre> stuff </pre>
    </div>
</div>
{% endhighlight %}

Unfortunately, they were located in posts.css, a minified CSS file that Posterous doesn't allow you to directly edit. Luckily, you can override style elements by redefining a class in the page's header. Using the "Customize" screen again I inserted:

{% highlight css %}
<style>
    .CodeRay {
        background-color: #3E3E3E;
        border: 0px solid #232323;
        font-family: 'Courier New','Terminal',monospace;
        color: #E6E0DB;
        padding: 3px 5px;
        overflow: auto;
        font-size: 12px;
        margin: 8px 0 8px 0;
        line-height: 14px;
        border-radius: 2px;
    }
</style>
{% endhighlight %}

Now I can pretty much style the blocks however I want. I'm thinking of even finding an alternative monospace font and using it via [Google Web Fonts](http://www.google.com/webfonts#ChoosePlace:select).
