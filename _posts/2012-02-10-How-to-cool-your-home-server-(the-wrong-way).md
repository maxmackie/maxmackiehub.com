---
layout: default
---

# How to cool your home server (the wrong way)
<p class="date">Friday February 10th, 2012</p>

Last week I used some spare parts I had laying around to build a little home server. There wasn't any planned use for it, other than maybe a local backup tool. I might but the specs online at a later date ... but that's not why I'm writing this post.

I installed a fresh version of Ubuntu on the server and got everything up and running. I started updating the OS for the first time (adding some non-free repos) and to my movie to help pass the time. It was about 20 minutes into Inception that the server started making a whole lot of noise ... like a worrying amount of noise.

Suspecting a temperature issue, I pulled up a terminal and used the handy `sensors` tool:

    max@skunkbox:~$ sensors
    acpitz-virtual-0
    Adapter: Virtual device
    temp1:        +79.0°C  (crit = +85.0°C)

Holy smokes! 79°C is tons of degrees warmer than it should be! Alarm bells started ringing and I didn't want to damage the CPU, so I pulled the plug on the server and took a second to think. I pulled the cover off the box and started it up again. To my surprise, the heatsink's fan was moving slower than I've ever seen one move (we're talking like 100 RPM).

I didn't have any extra heatsinks, the fan was an odd size and I was all out of thermal compound (like it would have really helped). So, I did the next best thing.

I give you, my patented server cooling system:

![hack all the things!](/img/2012-02-10-1.png)

Yup, that's right. A tabletop fan blowing into a high-speed case fan blowing into a low-speed heatsink fan. Now I'm running at a cool 29°C :)

See? Simple IS everything.
