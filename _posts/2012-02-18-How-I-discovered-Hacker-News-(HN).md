---
layout: default
---

# How I discovered Hacker News (HN)
<p class="date">Saturday February 18th, 2012</p>

The internet is a pretty big place.

Despite it being so big, quality news isn't so hard to come by. What becomes hard is sifting through bad and poorly written content and bringing out the good stuff.

For the longest time, my solution to this problem was to use Google Reader (and alternates) to gather feeds I slowly found on my perusal of the Web. While this works if you read small amounts of news, it fails when your hunger for information goes through the roof. The issue isn't with the platform, it's the fact that the only news sources you have are the ones you already know (which clearly limits what you read).

So, I was in search of a news aggregator. That's when I found [Hacker News](http://news.ycombinator.com/news).

HN is everything I could ask for. Simple design, which maximizes the information and gets rid of bulk, community driven content, intellectual commenting and a very strong sense of community. It's like Reddit on crack.

I quickly made an account and opened all the stories on the front page in new tabs. Sure, there were some that weren't exactly up my alley, but HN made up for that by giving me posts about software conventions and style, new tech, startups (a topic introduced to me by HN), social commentary, silicon valley, security breaches and more.

While this blog post isn't very informative and probably hasn't told you anything new, but I though it was necessary to thank the people who scour the internet and find the cool stuff for us lazy people to read (actually, being part of the community has driven me to start sharing what I find -- mission accomplished HN).

<sup>(thank you to everyone for getting this article on the front page of HN)</sup>
