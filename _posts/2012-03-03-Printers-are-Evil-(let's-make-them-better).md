---
layout: default
---

# Printers are Evil (let's make them better)
<p class="date">Saturday March 3rd, 2012</p>

My recent job acceptance required me to fill out a federal government security form. I received the form via email and was surprised to see it was one of those nifty "fill out the form in your browser" pdfs. I actually had fun filling out the papers (using Chrome's pdf viewer) and proceeded to print them out. (**Printer fail #1**: new cartridge is reporting as empty). I literally replaced the black ink the day before anticipating a higher volume of printing (I had some assignments coming up).

Not wanting to buy yet another ink cartridge ($25), I tried printing in dark blue. After all I only needed to copy/fax it back to the office, so dark colours wouldn't be that recognizable from black. (**Printer fail #2**: Unfortunately, my printer completely locks up if you need to replace a cartridge, so printing was impossible). This sucked, but wasn't the biggest deal. I got my girlfriend to print it at her work. 

I had now acquired the printed form! Muahaha printer, take that! It was time to scan it and email it back to HR along with a piece of government issued identification. However, the printer had one more trick up it's sleeve (**Printer fail #3**: An empty cartridge means I can't even scan something digitally!). There was no printing involved, no ink involved, all I needed was the scanner to send data to my desktop. I give up printer, you win.

I ended up rushing to the computer shop and buying some damn ink (my cartridge model was low in stock, so I was lucky). I replaced the ink and I was able to get my papers out to the office.

This whole ordeal compelled me to write about what I believe is fundamentally wrong with the current state of printers, and offer solutions that (hopefully) someone will listen to.

(I'd like to note that [The Oatmeal beat me to this with an awesome comic](http://theoatmeal.com/comics/printers))

![the oatmeal has quite a talent for evil printers](/img/2012-03-16-2.png)

**Standardize ink cartridge size and capacity**

A big problem with printers is ink. You might have gotten a great printer for a great price, but you'll end up buying ink until either you or your printer dies. Unfortunately, there is no actual way around this, but I do believe the situation could be made easier for the customer. The sheer number of proprietary cartridge sizes and capacities make it very difficult to find affordable and available ink when you need it. Why not create a cross-model standard ink cartridge size which would be compatible with all printers on the market? I understand big corporations (I'm looking at you HP, Epson and Canon) need to design their own form factors and heads to ensure their domination and profits, but why not do the "right" thing and design a common platform all printers can use? I may be a dreamer, but this would be a huge step in the right direction.

**Standardize printer drivers (cross platform, people!)**

Countless times I've had to dig around and find the correct drivers for my printer (like a lot of people, I'm in a complete Linux/Unix/FreeBSD environment). I might be on a different platform, but I'm still a customer and would love to have one driver which works with all my printers. I don't want to navigate your website and see if you support Linux, nor do I want to see if someone ported over your driver with CUPS. It's more work than it should be. Create one driver with one standard interface and leave it at that. Please people, think of the customer (the Linux ones too).

**Stop making stupid programming decisions**

There is absolutely no reason to prohibit the user of using their scanner if they don't have ink installed. This might only apply to my specific model, but I wouldn't put it past the major printer companies. I bought a printer/fax/scanner/copier combo, please let me use it as such. 

I digress. I'm brain is hard wired the open source philosophy and I embrace the notion of platform-wide standards. Not much technical progress has been made for consumer grade printers for several years, it might be time for mass printer reform.
