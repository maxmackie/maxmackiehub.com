---
layout: default
---

# Stop distributing .doc files
<p class="date">Monday March 19th, 2012</p>

`.doc` started as a file format used by many operating systems and used to store only plain-text. In the 90's, Microsoft decided to use the extension for Word -- their new word processor. Not only did they decide to use the extension, but they also broke the standard by using a proprietary format, instead of the plain-text most people were used to. Despite the `.doc` extension being more than **10 years old** and the fact that most documents formatted this way render horribly in operating systems other than Windows, I still regularly receive documents in this format. Stop it.

However aggravating the above paragraph is, this article isn't solely targeted at the `.doc` extension -- it is a public plea for people to stop distributing application specific documents and to start distrubting all documents in `.pdf`.

Yesterday I received an email from an engineering society I'm a part of. They were notifying all members of the upcoming annual general meeting that is taking place next week. I immediately lost interest in the email when I saw that their meeting schedule and their nomination form were both in `.docx` format. I don't like being forced to download the file, minimize my browser, launch an Office application (in my case, LibreOffice) and then read the content. Had it been distributed in `.pdf`, Chrome would have immediately opened a new tab for me displaying the document and giving me options to save, zoom or print it. I'm sure Firefox, Opera and (maybe) Internet Explorer have similar features.

We are increasingly collaborating on the web, and the need for bulky desktop applications are almost gone with the wind. Had I not had an office application on my computer (which could have been possible seeing as I do most of my documentation and report with LaTeX), I would have had an extra step before viewing the content that was sent to me. It's not like it is difficult either. All major office applications I know of allow easy exporting to `.pdf` file format and LibreOffice also allows you to save a hybrid `.pdf` and `.odt` together in one file. This allows you to distribute the PDF as is and also open it for editing.

There is virtually no need to distribute documents in any format, proprietary or not, other than PDF. Don't make your users work and fiddle with your platform-specific or application-dependent documents.

Comments: [Hacker News](http://news.ycombinator.com/item?id=3743590)
