---
layout: default
---

# Idea: Include web libraries right in your browser
<p class="date">Friday March 23rd, 2012</p>

We all strive for speed. Neck-breaking edge-slicing so-fast-it-seems-instant speed. But we're limited to the size of the webpages and scripts we download each time we visit a new site. Of course, there's [caching](http://www.stevesouders.com/blog/2012/03/22/cache-them-if-you-can/), [content compression](http://betterexplained.com/articles/how-to-optimize-your-site-with-gzip-compression/) and asset [minification](http://developer.yahoo.com/blogs/ydn/posts/2007/07/high_performanc_8/) but it's only really effective for dynamic content that MUST be served from the website itself.

More and more we see the creation and mass adoption of Javascript libraries like jQuery and Prototype. The cool thing about these libraries is that they're all static (not including a version change). This means that no matter which website you visit that uses jQuery, your browser will be using the same base library as everyone else. So if the content is static, and it's used on a ton of websites already, why do we keep re-downloading it?

I know next to nothing about browser design, but I do have an idea. It could be a piece of garbage (for some reason ignorant to me) or it could be really cool. _What if users could have the option to store libraries like jQuery client-side (on their computers) and have the browser replace an outgoing call for the script by the local one?_ Javascript is already run by the browser, so why are we asking a remote server for the same functions we use over and over again?

I do see a couple problems with this idea:

_Problem 1:_ You need to keep the libraries updated. I doubt that it would be possible to integrate updates for the libraries into the core browser updates, meaning "Updating Chrome" wouldn't necessarily update the library. This means it's up to the user to update it manually, which in turn means it will only really be adopted with people familiar with non-novice browser use. One could always create a browser plugin to manage the libraries, but that's a project in itself.

_Problem 2:_ You need to change how browsers interpret an outgoing call to include the library (for backwards compatibility). For example, most people will use Google's hosted version of jQuery on their sites. The browser would have to recognize that line of HTML and be smart enough to replace it with the local stored version of jQuery. I'm not sure this could be accomplished with an extension or not, but I assume it's possible.

From what I understand, if you use Google's AJAX Libraries CDN, you might not have to download jQuery for long periods of time due to some pretty awesome caching. But there are other libraries other than jQuery that would benefit from browser-side library inclusion. I think it would make large homebrew libraries and scripts a lot more attractive to a wider audience if they knew that users wouldn't be bogged down by the extra requests (not to mention saving a bunch of bandwidth on the developers' servers).

This is just an idea. It's very likely someone on HN will tell me this is all already being done and I'd be more than happy to hear it.

Comments: [Hacker News](http://news.ycombinator.com/item?id=3746401)
