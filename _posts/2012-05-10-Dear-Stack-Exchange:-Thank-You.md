---
layout: default
---

# Dear StackExchange: Thank You
<p class="date">Thursday May 10th, 2012</p>

I've been a frequent user of the [StackExchange (SE)](http://stackexchange.com/) network for about 2 years now. It started when I had difficult programming questions and was redirected to StackOverflow, and continued to grow until I had accounts on [41 different SE sites](http://stackexchange.com/users/310807/maxmackie?tab=accounts). I've never come across a better interactive learning tool in my life, I owe a lot to the communities that drive the SE sites.

Being a huge Linux fan, I was overjoyed to discover [Unix&Linux.SE (U&L)](http://unix.stackexchange.com/). I quickly started asking and answering questions and over time became very attached to the community and the great people behind it.

On March 2nd, Rebecca Chernoff (SE employee) announced that every user on U&L who appeared on the first two user pages (sorted by total reputation) would be receiving a complimentary "thank you" care package from the SE team for helping to build a great Linux and Unix community site. The kit was said to include a U&L t-shirt, some U&L stickers, and a nice SE pen and sharpie. I was excited and happy that the administration recognized the huge contributions that users bring to their network.

I got the package in the mail about 2-3 weeks later, and was surprised to see a hand-signed letter from [Joel Spolsky](http://www.joelonsoftware.com/AboutMe.html) (the CEO of StackExchange). The letter was typed up and most likely contained a generic message (my username was never mentioned), but the fact that they would send a hand-signed (no photocopy crap here) letter like that on top of all the other swag was amazing. They WOW'ed me and exceeded my expectations ... again.

I think Joel and the whole StackExchange team deserves this post. Thank you very much, you make the internet a better place.

[Comments (HN)](http://news.ycombinator.com/item?id=3964246)
