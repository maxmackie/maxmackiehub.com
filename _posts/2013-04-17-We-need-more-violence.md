---
layout: default
---

# We Need More Violence
<p class="date">Wednesday April 17th, 2013</p>

I've never been a big fan of the news, whether it be in print or televised. As a recent article on Hacker News pointed out, there's nothing much to gain by consuming report after report. In fact, modern news sickens me.

Society seems to have an unsatisfied craving for violence and hate, which news outlets keep pouring out to keep the general audience's attention. The Boston bombings were a horrible act of hate, in-humanism and cowardice, but seeing images of victims with severed limbs and giant pools of blood in the streets is not my preferred method of getting information about the incident. Don't get me wrong. I'm against censorship and I don't think news outlets are the bad guys for wanting to publish these graphic images. I think that people, _us_, have come to expect such levels of violence as regular occurrences.

Why is that? Publishing images like the ones hitting the press in the past couple of days would have been unheard of a couple years back. This is not a rant, but an open ended question:

> (Why) Do we need more violence?


[Comments (HN)](https://news.ycombinator.com/item?id=5565406)
