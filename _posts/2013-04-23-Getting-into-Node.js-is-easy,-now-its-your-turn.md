---
layout: default
---

# Getting into Node.js is easy, now its your turn
<p class="date">Tuesday April 23rd, 2013</p>

It's been a busy week for me. Late last Friday I have an awesome idea for a tick-based browser-based scifi ship-building game (try saying that twice).

Instead of the usual stack (PHP, MySQL and Apache), I decided to jump feet first into something foreign to me: Node.js. I spend a lot of time on Hacker News so it was only a matter of time before I felt like enough of an outsider to try it out (so many damn posts about Angular and Ember). Well, I've been pleasantly surprised.

Node is awesome. Seriously awesome. With very little knowledge of javascript, I was able to use `npm` to install `express` (a web framework for node) and spin up a node server. After that, integrating MongoDB (whoa, I'm actually using a NoSQL database) and a couple other plugins was very simple.

If you're still unsure about taking the plunge into the node world, **do it**! Get your feet wet and maybe a little dirty and you won't be disappointed.

*On a side note, I figure a good way to share what I learn along the way will be to post some tutorials -- if you're interested, stay tuned.*
