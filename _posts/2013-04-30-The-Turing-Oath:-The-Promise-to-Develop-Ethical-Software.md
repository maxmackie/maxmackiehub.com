---
layout: default
---

# The Turing Oath: The Promise to Develop Ethical Software
<p class="date">Tuesday April 30th, 2013</p>

In light of some [recent unethical behaviour in software development](http://www.branded3.com/blogs/the-antisocial-network-path-texts-my-entire-phonebook-at-6am/), and after a [user on Hacker News](https://news.ycombinator.com/item?id=5633551) suggested that software developers have their own "version" of a Hippocratic Oath, I figured I'd try my hand at drafting an initial attempt.

The oath deals with user privacy and ethical handling of their personal information. This primarily caters to web applications which hold the personal information of users. It's named after [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing) because he was a remarkable person who advanced computer science by great leaps.

The oath is hosted on [Github](https://github.com/maxmackie/Turing-Oath/blob/master/README.md) and I encourage people to fork it and edit its contents. Add what you think ethical software development is.

#### The Turing Oath

1. I swear to respect the privacy of the user. All user information kept on web servers should be properly protected, encrypted and secured.

2. I swear to not invade the private space of the user. Users will not be spammed, contacted needlessly or have their personal information used without their knowledge.

3. I swear to be transparent in the information I keep on the user. Users should always know all the information that is currently kept on them and have access to it at all times.

4. I swear to release and remove user information at the user's request. Users should be allowed to leave a service or uninstall a product and have all stored personal information on them removed.
