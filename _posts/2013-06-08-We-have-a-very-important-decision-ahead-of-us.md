---
layout: default
---

# We have a very important decision ahead of us
<p class="date">Saturday June 8th, 2013</p>

Recent [leaks](http://thedocs.hostzi.com/) have revealed that the NSA has been collecting live data (emails, voice messages and most online activity for starters) on millions of people since 2007 in a program called [PRISM](https://en.wikipedia.org/wiki/PRISM_\(surveillance_program\)). This widespread disregard of basic privacy rights does not just affect US citizens, but encompasses information from all foreign visitors to the big American tech companies (Google, Microsoft, Skype and Yahoo to name a few).

In the days following the leak, the internet has exploded with discussions regarding the ethics of this program. Some internet giants have published [official](https://www.facebook.com/zuck/posts/10100828955847631) [statements](http://googleblog.blogspot.ca/2013/06/what.html) denying their participation in PRISM and accusing news outlets of exaggerating and confusing the facts. 

Regardless of the truth behind all these stories, it is clear that we are approaching a critical moment in internet and telecommunication privacy. We are currently losing a tug-of-war battle with world governments, and our freedoms are on the line. 

We have a very important decision ahead of us. 

Will we live in an [Orwellian](https://en.wikipedia.org/wiki/Orwellian) future where our opinions, communications and thoughts are monitored for our "well being"? Will we trust our governments with our precious freedoms under the guise of our own security? *Only if we let it come to that*. There is an immense body of open source, privacy-focused tools at our disposal -- all we have to do is use them.

Encrypting your communications isn't for the tinfoil hat wearing computer geeks anymore. These tools are accessible to the general public.If you're a developer, create open source encryption software. If you're an entrepreneur, make this your new market. If you care about privacy, let other people know. 

Relevant (and slightly overused) quote from the movie [Network (1976)](https://en.wikipedia.org/wiki/Network_film):

> We know things are bad — worse than bad. They're crazy. It's like everything everywhere is going crazy, 
> so we don't go out anymore. We sit in the house, and slowly the world we are living in is getting smaller, 
> and all we say is: 'Please, at least leave us alone in our living rooms. Let me have my toaster and my 
> TV and my steel-belted radials and I won't say anything. Just leave us alone.'
>
> Well, I'm not gonna leave you alone. I want you to get **mad**! I don't want you to protest. I don't want 
> you to riot — I don't want you to write to your congressman, because I wouldn't know what to tell you to 
> write. I don't know what to do about the depression and the inflation and the Russians and the crime in 
> the street. All I know is that first you've got to get mad. You've got to say: 'I'm a human being, 
> god-dammit! My life has value!'

Share your thoughts with me via email. You can find my public key [here](http://maxmackie.com/key.txt)