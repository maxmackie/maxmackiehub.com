---
layout: default
---

# Privacy tools we should all be using
<p class="date">Sunday June 9th, 2013</p>

> There are two types of encryption: one that will prevent your sister from reading your diary and one that will prevent your government.
>
> <p class="author">-Bruce Schneier</p>

Its no secret, [we have an important decision ahead of us](http://maxmackie.com/2013/06/08/We-have-a-very-important-decision-ahead-of-us/). The world that [tinfoil hat privacy advocates](https://en.wikipedia.org/wiki/Richard_Stallman) have been preaching is slowly becoming a reality. However, publicly available tools exist for the average citizen to begin taking appropriate steps to protect themselves from prying eyes. Its up to us to use the tools and take out privacy and security into our own hands.

I've compiled a list (by no means is it exhaustive) of software and tools every person should know about and use. Some are accessible to the technological layperson, some aren't. That's why its important for those who can use the more complex tools to educate those who can't.

You can protect your privacy (and your personal information) from three different fronts. Encrypting your data **locally** (on your computer and devices) will protect you from those with physical access to your machine. Masking and encrypting your **network** traffic protects you from those in a position to eavesdrop on your connection. Finally, you can encrypt the data **stored** on remote servers so the people running the services you're supposed to trust don't actually see what they're storing.

### Local Encryption

One of the best options for protecting your local data is to utilize full disk encryption. Native solutions exist for [OSX](https://en.wikipedia.org/wiki/FileVault) and most [Linux](https://www.eff.org/deeplinks/2012/11/privacy-ubuntu-1210-full-disk-encryption) [distributions](https://wiki.archlinux.org/index.php/Disk_Encryption) and are usually very easy to set up. Windows users can use [Bitlocker](https://en.wikipedia.org/wiki/BitLocker_Drive_Encryption), a disk encryption tool made by Microsoft, but some have voiced security concerns that there may be a silent backdoor in the software. Because of this, Windows users are advised to use a tool like [TrueCrypt for their system encryption needs](http://www.truecrypt.org/docs/?s=system-encryption).

However, full disk encryption is not fool-proof. An attacker can access encryption keys if they have access to a running computer by using a vulnerability known as a [cold boot attack](https://en.wikipedia.org/wiki/Cold_boot_attack). If the machine isn't running, it is still possible to get the keys by exploiting the [pre-boot authentication](https://en.wikipedia.org/wiki/Pre-Boot_Authentication) system. 

In addition to encrypting your hard drive, you can use [TrueCrypt](http://www.truecrypt.org/) to create secure virtual disk within a file on your computer. The disk could hold sensitive information such as passwords, banking info and contacts.

### Network Encryption and Anonymity

Once your data leaves your computer its vulnerable from a whole slew of different snooping vectors. 

The first step to securing your online communications is to connect to websites using the [HTTPS protocol](https://en.wikipedia.org/wiki/Https), instead of the unencrypted HTTP one. The caveat is that the website you're connecting to must have an SSL certificate set up to use the protocol -- some of which don't. You're best option is to install the [HTTPS Everywhere](https://www.eff.org/https-everywhere) plugin for your browser (thanks to the EFF and Tor people for it). This plugin will check each connection you make and attempt to recreate it using HTTPS if it already isn't. If you use a website that doesn't support HTTPS, try looking for an alternative or contact their support staff to inquire if they have plans of implementing it.

In addition to HTTPS, you can use [Tor]() to protect your anonymity while browsing. Tor routes your traffic through a huge network of other tor servers (known as the tor network) effectively masking your origin (ip address) from everyone along the way (except for the end point of the connection). The tor bundle is available for all major platforms ([OSX](https://www.torproject.org/download/download-easy.html.en#mac), [Linux](https://www.torproject.org/download/download-easy.html.en#linux) and [Windows](https://www.torproject.org/download/download-easy.html.en#windows)) and is very simple to use.

For an easy to understand infographic about the technologies presented above, see the [EFF's Tor and HTTPS](https://www.eff.org/pages/tor-and-https) page.

There are other tools currently under development that take the above to a whole new level. Projects are being worked on to create [peer-driven mesh networks](https://projectmeshnet.org/) that are completely decentralized. While these efforts are still in their infancy, recent events will surely drive new talent to the communities.

### Remote Encryption

The final destination of your data is the website or service you're connecting to. Protecting your data in this environment is much harder and relies on trust between you and the owners of the website. If you can't trust a website with your personal information, **don't use it**. This is the most powerful method available to protect your privacy. 

If you rely on a cloud service (storage, for example) that doesn't respect your privacy, find one that does, or move to use only [zero-knowledge](https://www.clipperz.com/blog/2007/08/24/anatomy_zero_knowledge_web_application/) web applications. If you are technologically able, [host your own cloud service](https://encrypted.google.com/search?q=host+personal+cloud) and get your family and friends using it. Companies should be storing your data in an encrypted state such that they would never be able to decrypt it without you (they don't hold the keys). You can use tools to encrypt your data locally before uploading them to the cloud as a last resort. Online email services are of a particular interest to data miners and eavesdroppers. I would recommend using [Hushmail](https://www.hushmail.com/) or running a local mail server at home.

*Assume that any information you post online is being tracked and analyzed by anyone and everyone. We have the tools to improve our privacy online -- we just need to use them.*
