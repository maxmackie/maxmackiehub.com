---
layout: default
title: About
---

Resume
------
My full resume can be found on [StackExchange Careers](http://careers.stackoverflow.com/maxmackie). A list of references will be provided on contact. I am not currently looking for work (school and internships), but send me an email and we can talk.

Contact
-------
My preferred method of contact is by email using this address: [talk@f33r.com](mailto:talk@f33r.com)

If you prefer private communication, please use my [public key](key.txt) to encrypt your email.
