---
layout: default
title: Projects
---

The Turing Oath
---------------
The Turing Oath is an attempt at an open-source ethics oath for developers. You can view the [Github repository](https://github.com/maxmackie/Turing-Oath) here.

Swatches
--------
<a href="/jquery.swatches"><img class="thumb" src="https://raw.github.com/maxmackie/jquery.swatches/master/img/beach.png" align="center"></a>

Swatches is a jQuery plugin that turns a one-line div into a sweet color swatch. Take a look at the [repository](http://github.com/maxmackie/jquery.swatches) and the [demo site](http://maxmackie.com/jquery.swatches) for more info.

BeepEvery.com
-------------
<a href="http://beepevery.com"><img class="thumb" src="http://oi43.tinypic.com/aopba0.jpg" align="center"></a>

A weekend project that beeps to your command. Possible applications? Maybe. Check out the [repository](http://github.com/maxmackie/beepevery).



